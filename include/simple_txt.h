/************************************************************************//**
 * Simple Text: allows loading bitmap fonts and using them to render text
 * to a buffer.
 *
 * Loaded fonts must be 1 bit per pixel (and thus use small flash space).
 * For the font, it is recommended that the first character in the font is the
 * space (' ', 0x20), and any following characters use the standard ASCII
 * ordering. Otherwise printing string literals with st_draw() can get tricky.
 * Text is rendered to a 24 bit per pixel buffer.
 ****************************************************************************/

#ifndef _SIMPLE_TXT_H_
#define _SIMPLE_TXT_H_

#include <stdint.h>

/// Simple Text font configuration
struct st_cfg {
	const uint8_t *font_data;	///< Raw font data, 1 bit per pixel
	const uint8_t *font_spacing;	///< Spacing for each font character
	uint32_t num_characters;	///< Number of characters in font
	uint32_t font_data_width;	///< font_data variable width in px
	uint32_t font_data_height;	///< font_data variable height in px
};

/// Colors (foreground/background) used to render text
struct st_color {
	uint8_t fg[3];	// Foreground color, 24-bit per pixel
	uint8_t bg[3];	// Background color, 24-bit per pixel
};

/// Font handler
typedef struct st_font* st_font_handle;

#ifdef __cplusplus
extern "C" {
#endif

/************************************************************************//**
 * \brief Load a font, return a handler to use it.
 *
 * \param[in] font_cfg Font configuration.
 *
 * \return The font handler, or NULL if not enough memory is available.
 ****************************************************************************/
st_font_handle st_font_load(const struct st_cfg *font_cfg);

/************************************************************************//**
 * \brief Unload a previously loaded font, freeing its resources.
 *
 * \param[in] font The handler of the font to unload.
 ****************************************************************************/
void st_font_unload(st_font_handle font);

/************************************************************************//**
 * \brief Returns the number of columns required to render a message with
 * the specified font.
 *
 * This function can be used to verify if message fits in a static buffer or
 * to allocate a dynamic buffer prior to rendering a message.
 *
 * \param[in] font Handler for the font used to render the text.
 * \param[in] text Text to get the number of columns required.
 *
 * \return The number of columns required to render the text with specified
 * font.
 ****************************************************************************/
int st_cols_get(st_font_handle font, const char *text);

/************************************************************************//**
 * \brief Renders a message with the specified font to a frame buffer.
 *
 * \param[in]  font      Handler for the font used to render the text.
 * \param[in]  text      Text to render.
 * \param[in]  color     Color used to render the text.
 * \param[out] buffer    Buffer to which text will be rendered.
 * \param[in]  buf_width Width of a single buffer line, in bytes.
 *
 * \return The number of columns required to render the text with specified
 * font.
 ****************************************************************************/
int st_draw(st_font_handle font, const char *text,
		const struct st_color *color, uint8_t *buffer,
		uint32_t buf_width);

#ifdef __cplusplus
}
#endif

#endif /*_SIMPLE_TXT_H_*/

