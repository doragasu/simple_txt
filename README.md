# Introduction

This module allows rendering text to a 24-bit per pixel buffer, using predefined 1-bit per pixel fonts. Fonts can have any number of characters and each character can be any number of pixels wide. It is prepared to be used with Espressif SDKs ([ESP8266\_RTOS\_SDK](https://github.com/espressif/ESP8266_RTOS_SDK) and [esp-idf](https://github.com/espressif/esp-idf)), but its implementation is pretty generic and has no dependencies other than standard library functions, so you should be able to use it with any device.

# Usage

You can see an example on how to use this module along with the [scroller](https://gitlab.com/doragasu/scroller) module in the [m5am\_text\_scroll](https://gitlab.com/doragasu/m5am_text_scroll) project.

1. Load a font using `st_font_load()`. Font must be 1-bit per pixel, and all the characters must be disposed one after another.
2. Prepare the destination buffer: either use a statically pre-allocated buffer or allocate a dynamic one. The `st_cols_get()` function can be used to obtain the number of required columns in the buffer. Required buffer space is `3 * cols * rows`.
3. Draw the text to the buffer. Call `st_draw()` to do the job. You will have to give it the loaded font handler, the message, the color (foreground and background), the buffer and the number of bytes in a buffer row (basically, 3 times the number of pixel columns in the buffer).

Once you have the buffer ready, use whatever function you like to draw it to a screen, for example my [scroller module](https://gitlab.com/doragasu/scroller).

# License

This module is provided with NO WARRANTY under the [Mozilla Public License (MPL)](https://www.mozilla.org/en-US/MPL/).

