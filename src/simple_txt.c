#include <stdlib.h>

#include <simple_txt.h>

/// Return nonzero if bit at array position pos is one, zero otherwise
/// NOTE: We could consider having a LUT with precomputed values, but it can
/// get big pretty fast
#define BIT(array, pos) ((array)[(pos)>>3] & (1<<(7 - ((pos) & 7))))

struct st_font {
	struct st_cfg cfg;
	uint32_t off[];
};

st_font_handle st_font_load(const struct st_cfg *font_cfg)
{
	const uint32_t num_chars = font_cfg->num_characters;
	const uint32_t data_len = sizeof(struct st_font) +
		num_chars * sizeof(uint32_t);
	st_font_handle font = malloc(data_len);

	if (!font) {
		return NULL;
	}

	font->cfg = *font_cfg;

	for (uint32_t i = 0, off = 0; i < num_chars; i++) {
		font->off[i] = off;
		off += font_cfg->font_spacing[i];
	};

	return font;
}

void st_font_unload(st_font_handle font)
{
	free(font);
}

int st_cols_get(st_font_handle font, const char *text)
{
	int cols = 0;

	for (int i = 0; text[i]; i++) {
		cols += font->cfg.font_spacing[text[i] - ' '];
	}

	return cols;
}

static uint8_t *draw_px(uint8_t *dst, const uint8_t color[3])
{
	*dst++ = color[0];
	*dst++ = color[1];
	*dst++ = color[2];

	return dst;
}

static int chr_copy(st_font_handle font, uint8_t *dst, uint32_t dst_width,
		char chr, const struct st_color *color)
{
	const uint32_t chr_idx = chr - ' ';
	const uint8_t spacing = font->cfg.font_spacing[chr_idx];
	uint32_t off = font->off[chr_idx];

	for (int i = 0; i < font->cfg.font_data_height; i++) {
		uint8_t *tmp = dst;
		for (uint32_t j = 0; j < spacing; j++) {
			if (BIT(font->cfg.font_data, off + j)) {
				tmp = draw_px(tmp, color->fg);
			} else {
				tmp = draw_px(tmp, color->bg);
			}
		}
		// Go to next line of font and buffer
		off += font->cfg.font_data_width;
		dst += dst_width;
	}

	return spacing * 3;
}

int st_draw(st_font_handle font, const char *text,
		const struct st_color *color, uint8_t *buffer,
		uint32_t buf_width)
{
	int pos = 0;

	for (int i = 0; text[i]; i++) {
		pos += chr_copy(font, buffer + pos, buf_width, text[i], color);
	}

	return pos;
}

